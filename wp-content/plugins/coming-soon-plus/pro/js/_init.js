jQuery(document).ready(function() {
	var vColorPickerOptions = {
    	defaultColor: false,
    	change: function(event, ui){},
    	clear: function() {},
		hide: true,
		palettes: true
	};
	jQuery('#color-countdown').wpColorPicker(vColorPickerOptions);
	var max_date = new Date();
		max_date.setDate(max_date.getDate()+99);
	jQuery('#expiry_date').datepicker({
		 changeMonth: true,
		 changeYear:  true,
		 minDate: new Date(),
		 maxDate: max_date,
		 dateFormat: "dd/mm/yy" 
	});
	
	jQuery('#clear_date').on('click', function() {
		jQuery('#expiry_date').removeAttr('readonly');
		jQuery('#expiry_date').val('');
		jQuery('#expiry_date').attr('readonly','readonly');
		return false;
	});
});
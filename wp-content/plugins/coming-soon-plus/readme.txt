=== Site Offline/Coming Soon Premium ===
Contributors: wpecommerce
Tags: coming soon,site is down,maintenance,teaser,offline,google
Requires at least: 3.0
Tested up to: 3.9
Stable tag: 2.1.2
License: GPLv2 or later

== Description ==

Use this plugin to enable/disable site offline mode. When Site offline Mode is enabled regular visitors of your site will see a page(you can edit that page by going to "Maintenance" from the Dashboard) and you as a admin can make and test safe changes to your site. Very helpful for those developers who need coming soon pages for there sites.

For documentation visit the <a href="http://wp-ecommerce.net/wp-coming-soon-plugin-easily-create-a-coming-soon-page-for-your-wordpress-blog-2328">WordPress Site offline/Coming Soon Plugin</a>
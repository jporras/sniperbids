<?php

/**
* This will return the markup of notifications settings form fields.
*/
function ultdww_user_notifications_settings()
{
	$current_user = wp_get_current_user();

	$notify1 = (get_user_meta( $current_user->ID, 'ultdww_notify_when_outbid', true )) ? 'checked': '';

	$notify2 = (get_user_meta( $current_user->ID, 'ultdww_notify_when_ended', true )) ? 'checked': '';

	$html = '<hr />
	<div id="notifications-settings">
	    <div class="settings-heading">
	    	<h5>Notifications</h5>
	    </div>    
    	<div class="notify-when-outbid">
			<input value="1" '.$notify1.' name="ultdww_notify_when_outbid" type="checkbox"> Notify me when my auction outbid.
		</div>
		<div class="notify-when-ended">
			<input value="1" '.$notify2.' name="ultdww_notify_when_ended" type="checkbox"> Notify me when my auction ended.
		</div>
    </div>
    <!--.notifications-settings ends-->';


    return $html;
}

/**
* Save the notifications settings fields
*/
function ultdww_user_notifications_settings_save()
{
	$current_user = wp_get_current_user();

	if(isset($_POST['ultdww_notify_when_outbid']) AND $_POST['ultdww_notify_when_outbid'] == 1)
	{
		update_user_meta($current_user->ID, 'ultdww_notify_when_outbid', $_POST['ultdww_notify_when_outbid']);
	}

	if(isset($_POST['ultdww_notify_when_ended']) AND $_POST['ultdww_notify_when_ended'] == 1)
	{
		update_user_meta($current_user->ID, 'ultdww_notify_when_ended', $_POST['ultdww_notify_when_ended']);
	}
}
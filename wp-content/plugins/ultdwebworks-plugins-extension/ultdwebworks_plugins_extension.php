<?php
/*
Plugin Name: UltdWebWorks Plugins Extension
Plugin URI:  http://unlimitedwebworks.guru
Description: This plugin extends plugins inside wordpress application.
Version:     1.0
Author:      Jur Erick Porras
Author URI:  http://portfolio.unlimitedwebworks.guru
*/

define( 'ULTDWEBWORKS_PATH', dirname(__FILE__));

// include buddypress extension scripts

require ( ULTDWEBWORKS_PATH . '/buddypress/classes/class-ultdww-xprofile-field-type.php' ); 
require ( ULTDWEBWORKS_PATH . '/buddypress/classes/class-ultdww-xprofile-field-type-radiobutton.php' ); 
require ( ULTDWEBWORKS_PATH . '/buddypress/classes/class-ultdww-xprofile-field-type-textbox.php' ); 
require ( ULTDWEBWORKS_PATH . '/buddypress/classes/class-ultdww-xprofile-field-type-selectbox.php' ); 
require ( ULTDWEBWORKS_PATH . '/buddypress/functions.php' ); 

// include ultimate auction pro developer extension scripts
require ( ULTDWEBWORKS_PATH . '/ultimate-auction-pro-dev/settings-notifications.php' ); 
require ( ULTDWEBWORKS_PATH . '/ultimate-auction-pro-dev/functions.php' ); 

/**
* Dump data - this is for debugging.
*/
function dd($data)
{
	var_dump($data);
	die();
}


/**
* This will replace the registered permalink to it's associated link
* @param target url
*/

add_action('init','ultdww_menu_replace_permalink');

function ultdww_menu_replace_permalink($url){

	$current_user = wp_get_current_user();

	$logged_username = ($current_user->user_login) ? $current_user->user_login : '';

	$registered_permalinks = [
		'%username%' => $logged_username
	];

	foreach ($registered_permalinks as $pattern=>$permalink) {

		$url = str_replace($pattern, $permalink, $url);
	}
	
	return $url;
}	


function arphabet_widgets_init() {

	register_sidebar( array(
		'name'          => 'Content widgets',
		'id'            => 'content_widgets',
		'before_widget' => '<div class="row columns">',
		'after_widget'  => '</div>',
		'before_title'  => '<h2>',
		'after_title'   => '</h2>',
	) );

}
add_action( 'widgets_init', 'arphabet_widgets_init' );


function ultdww_leadingzero_num($no)
{
	// return sprintf('%02d', $no);
	return str_pad($no, 2, '0', STR_PAD_LEFT);
}

// ultimate auction pro developer extension

function ultdwebworks_ending_time_calculator($seconds, $end_tm) {

	$days = floor($seconds / 86400);
	$seconds %= 86400;

	$hours = floor($seconds / 3600);
	$seconds %= 3600;

	$minutes = floor($seconds / 60);
	$seconds %= 60;

	$rem_tm = '';
	$rem_dy = '';
	$rem_hr = '';
	$rem_min = '';
	$rem_sec = '';

	if ($end_tm == 'end_time') {

		// days
		// if ($days == 1 || $days == -1) 
		// {
		// 	$rem_dy = "<span class='wdm_datetime' id='wdm_days'>" . ultdww_leadingzero_num($days) . "</span> Day";
		// } 
		// elseif ($days == 0) 
		// {
		// 	$rem_dy = "<span class='wdm_datetime' id='wdm_days' style='display:none;'>" . ultdww_leadingzero_num($days) . "</span> Day";
		// } 
		// else 
		// {
		// 	$rem_dy = "<span class='wdm_datetime' id='wdm_days'>" . $days . "</span> Days";
		// }

		if ($days == 1 || $days == -1) 
		{
			$rem_dy = "<span class='wdm_datetime' id='wdm_days'>1</span> Day";
		} 
		elseif ($days == 0) 
		{
			$rem_dy = "<span class='wdm_datetime' id='wdm_days' style='display:none;'>0</span>";
		} 
		else 
		{
			$rem_dy = "<span class='wdm_datetime' id='wdm_days'>" . $days . "</span> Days";
		}


		// hours
		if ($hours == 1 || $hours == -1) 
		{
			$rem_hr = "<span class='wdm_datetime' id='wdm_hours'>" . ultdww_leadingzero_num($hours) . "</span>";
		} 
		elseif ($hours == 0) 
		{
			$rem_hr = "<span class='wdm_datetime' id='wdm_hours' style='display:none;'>" . ultdww_leadingzero_num($hours) . "</span>";
		} 
		else 
		{
			$rem_hr = "<span class='wdm_datetime' id='wdm_hours'>" . ultdww_leadingzero_num($hours) . "</span>";
		}

		// minutes
		if ($minutes == 1 || $minutes == -1) 
		{
			$rem_min = "<span class='wdm_datetime' id='wdm_minutes'>" . ultdww_leadingzero_num($minutes) . "</span>";
		} 
		elseif ($minutes == 0) 
		{
			$rem_min = "<span class='wdm_datetime' id='wdm_minutes' style='display:none;'>" . ultdww_leadingzero_num($minutes) . "</span>";
		} 
		else 
		{
			$rem_min = "<span class='wdm_datetime' id='wdm_minutes'>" . ultdww_leadingzero_num($minutes) . "</span>";
		}

		// seconds
		if ($seconds == 1 || $seconds == -1) 
		{
			$rem_sec = "<span class='wdm_datetime' id='wdm_seconds'>" . ultdww_leadingzero_num($seconds) . "</span>";
		} 
		elseif ($seconds == 0) 
		{
			$rem_sec = "<span class='wdm_datetime' id='wdm_seconds' style='display:none;'>" . ultdww_leadingzero_num($seconds) . "</span>";
		} 
		else 
		{
			$rem_sec = "<span class='wdm_datetime' id='wdm_seconds'>" . ultdww_leadingzero_num($seconds) . "</span>";
		}

		$rem_tm = $rem_dy . ' ' . $rem_hr . ':' . $rem_min . ':' . $rem_sec;

	} else {

		if ($days > 1) {
			$rem_dy = sprintf(__('%s days', 'wdm-ultimate-auction'), $days) . ' ';
		} elseif ($days == 1) {
			$rem_dy = sprintf(__('%s day', 'wdm-ultimate-auction'), $days) . ' ';
		}

		if ($hours > 1) {
			$rem_hr = sprintf(__('%s hours', 'wdm-ultimate-auction'), $hours) . ' ';
		} elseif ($hours == 1) {
			$rem_hr = sprintf(__('%s hour', 'wdm-ultimate-auction'), $hours) . ' ';
		}

		if ($minutes > 1) {
			$rem_min = sprintf(__('%s minutes', 'wdm-ultimate-auction'), $minutes) . ' ';
		} elseif ($minutes == 1) {
			$rem_min = sprintf(__('%s minute', 'wdm-ultimate-auction'), $minutes) . ' ';
		}

		if ($seconds > 1) {
			$rem_sec = sprintf(__('%s seconds', 'wdm-ultimate-auction'), $seconds);
		} elseif ($seconds == 1) {
			$rem_sec = sprintf(__('%s second', 'wdm-ultimate-auction'), $seconds);
		}

		if ($days >= 1) {
			$rem_tm = $rem_dy;
		} elseif ($days < 1) {
			if ($hours >= 1) {
				$rem_tm = $rem_hr;
			} elseif ($hours < 1) {
				if ($minutes >= 1) {
					$rem_tm = $rem_min;
				} elseif ($minutes < 1) {
					if ($seconds >= 1) {
						$rem_tm = $rem_sec;
					}

				}
			}
		}
	}

	return $rem_tm;
}

/**
* Display auction top links
* This function is dependent in the ff plugins:
* -Facebook Like
* -Ultimate Auction
*/ 

function ultdwebworks_display_like_btn(){

	$chk_watchlist = "n";

	$id = get_the_ID();

	if ( is_user_logged_in() ) {

		$auction_to_watch	 = $id;

		$cur_usr_id			 = get_current_user_id();

		$watch_auctions		 = get_user_meta( $cur_usr_id, 'wdm_watch_auctions' );

		if ( isset( $watch_auctions[0] ) ) {
			$watch_arr = explode( " ", $watch_auctions[ 0 ] );
			if ( in_array( $auction_to_watch, $watch_arr ) ) {
				$chk_watchlist = "y";
			}
		}
	}

	$html = '<ul class="menu ultdwww-auction-toplinks">';

	$html .= "<li class='ultdww-fb-like'>" . ultdwebworks_get_fb_like_btn() . "</li>";
	
	$html .= "<li>";

		if ( $chk_watchlist === 'n' ) {
			
			$html .= '<div class="wdm_add_to_watch_div_lin" id="wdm_add_to_watch_div_lin">
				<a class="button success" href="#" id="wdm_add_to_watch_lin" name="wdm_add_to_watch_lin">';
			$html .= 'Add to Watchlist';
			$html .='</a>
			</div>';

			$html .= '<div class="wdm_rmv_frm_watch_div_lin" id="wdm_rmv_frm_watch_div_lin" style="display:none;">
				<a class="button success" href="#" id="wdm_rmv_frm_watch_lin" name="wdm_rmv_frm_watch_lin">';
			$html .= 'Remove from Watchlist';
			$html .= '</a>
			</div>';

		} else {

			$html .= '<div class="wdm_add_to_watch_div_lin" id="wdm_add_to_watch_div_lin" style="display:none;">
				<a class="button success" href="#" id="wdm_add_to_watch_lin" name="wdm_add_to_watch_lin">';
			$html .= 'Add to Watchlist';
			$html .= '</a>
			</div>';

			$html .= '<div class="wdm_rmv_frm_watch_div_lin" id="wdm_rmv_frm_watch_div_lin">
				<a class="button success" href="#" id="wdm_rmv_frm_watch_lin" name="wdm_rmv_frm_watch_lin">';
			$html .= 'Remove from Watchlist';
			$html .= '</a>
			</div>';
		}

	$html .= "</li>";
	
	$html .= "<li><a class='small button secondary'>Report Auction</a></li>";

	$html .= "</ul>";

	echo $html;
}

function ultdwebworks_get_fb_like_btn(){
	global $posts;

	$layout=get_option('fblike_layout');

	$showface="false";
	if(get_option('fblike_showfaces')==1)
	{
		$showface="true";
	}

	if(get_option('fb_width')!="")
	{
		$width=get_option('fb_width');
	}
	else
	{
		$width=130;
	}
	if(get_option('fb_height')!="")
	{
		$height=get_option('fb_height');
	}
	else
	{
		$height=130;
	}

	$action=get_option('fblike_action');
	$font=get_option('fblike_font');
	$colorscheme=get_option('fblike_colorscheme');

	$iframe='<iframe src="http://www.facebook.com/plugins/like.php?locale='.get_option('fblike_display_lang').'&href='.urlencode(get_permalink($post->ID)).'&amp;layout='.$layout.'&amp;show-faces='.$showface.'&amp;width='.$width.'&amp;action='.$action.'&amp;colorscheme='.$colorscheme.'" scrolling="no" frameborder="0" allowTransparency="true" style="border:none; overflow:hidden; width:'.$width.'px; height:'.$height.'px"></iframe>';
	return $iframe;
}

/**
* Get the user membership type
*/
function ultdww_get_membership_type()
{
	global $current_user;

	$member_type_xfield_id = 10;

	return xprofile_get_field_data($member_type_xfield_id, $current_user->ID);
}

/**
* Display an alert content based from the membership types auction post permission. This is for add auction page in user dashboard.
*/
function ultdww_post_auction_member_permission_alert() 
{
	global $current_user;

	$message = '';

	if(isset($_GET['dashboard']) AND $_GET['dashboard'] == 'add-auction')
	{
		$xfield = ultdww_get_membership_type();

		switch ($xfield) {
			case 'Merchant $100':
				break;

			case 'Power Seller $50':
				break;
			
			default:
				
				$limit = 5;

				$args = array(
	                'posts_per_page'  => -1,
	                'post_type'       => 'ultimate-auction',
	                'auction-status'  => 'live',
	                'author'	=> $current_user->ID,
                );

                $auction_item_array = get_posts( $args );

                if( count($auction_item_array) >= $limit)
                {
                	$message = "<p>You have reached the limit of 5 auctions live at one time.</p>";
                }

				break;
		}
	}

	return $message;
}




// Twitter Oauth

// require ( ULTDWEBWORKS_PATH . '/twitter_oauth.php' ); // Block object

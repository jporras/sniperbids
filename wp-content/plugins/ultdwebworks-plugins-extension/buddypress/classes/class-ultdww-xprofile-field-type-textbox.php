<?php

/**
* see: plugins\buddypress\bp-xprofile\classes\class-bp-xprofile-field-type-textbox.php
*/
class ULTDWW_XProfile_Field_Type_Textbox extends ULTDWW_XProfile_Field_Type{

	public function edit_field_html( array $raw_properties = array()  )
	{
		// User_id is a special optional parameter that certain other fields
		// types pass to {@link bp_the_profile_field_options()}.
		if ( isset( $raw_properties['user_id'] ) ) {
			unset( $raw_properties['user_id'] );
		}

		$r = bp_parse_args( $raw_properties, array(
			'type'  => 'text',
			'value' => bp_get_the_profile_field_edit_value(),
		) ); ?>

		<div class="row">
			
			<div class="small-3 columns">
				<label for="<?php bp_the_profile_field_input_name(); ?>">
					<?php bp_the_profile_field_name(); ?>
					<?php bp_the_profile_field_required_label(); ?>
				</label>
			</div>

			<div class="small-9 columns">

				<?php

				/** This action is documented in bp-xprofile/bp-xprofile-classes */
				do_action( bp_get_the_profile_field_errors_action() ); ?>

				<input <?php echo $this->get_edit_field_html_elements( $r ); ?>>

			</div>
		</div>

		<?php
	}
}
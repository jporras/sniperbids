<?php

/**
* see: plugins\buddypress\bp-xprofile\classes\class-bp-xprofile-field-type.php
*/

class ULTDWW_XProfile_Field_Type{

	public function get_edit_field_html_elements(  array $properties = array()  )
	{
		$r = bp_parse_args( $properties, array(
			'id'   => bp_get_the_profile_field_input_name(),
			'name' => bp_get_the_profile_field_input_name(),
		) );

		if ( bp_get_the_profile_field_is_required() ) {
			$r['aria-required'] = 'true';
		}

		/**
		 * Filters the edit html elements and attributes.
		 *
		 * @since 2.0.0
		 *
		 * @param array  $r     Array of parsed arguments.
		 * @param string $value Class name for the current class instance.
		 */
		$r = (array) apply_filters( 'bp_xprofile_field_edit_html_elements', $r, get_class( $this ) );

		return bp_get_form_field_attributes( sanitize_key( bp_get_the_profile_field_name() ), $r );
	}
}
<?php


/**
 * Represents a type of Buddypress XProfile field and holds meta information about the type of value that it accepts.
 * see: plugins\buddypress\bp-xprofile\classes\class-bp-xprofile-field-type-radiobutton.php
 */

class ULTDWW_XProfile_Field_Type_Radiobutton{

	/**
	 * Output the edit field HTML for this field type.
	 *
	 * Must be used inside the {@link bp_profile_fields()} template loop.
	 *
	 * @since 2.0.0
	 *
	 * @param array $raw_properties Optional key/value array of
	 *                              {@link http://dev.w3.org/html5/markup/input.radio.html permitted attributes}
	 *                              that you want to add.
	 */
	public function edit_field_html( array $raw_properties = array() ) {

		// User_id is a special optional parameter that we pass to
		// {@link bp_the_profile_field_options()}.
		if ( isset( $raw_properties['user_id'] ) ) {
			$user_id = (int) $raw_properties['user_id'];
			unset( $raw_properties['user_id'] );
		} else {
			$user_id = bp_displayed_user_id();
		} ?>

		<div class="radio">

			<div class="row">

				<div class="small-3 columns">
					<label for="<?php bp_the_profile_field_input_name(); ?>">
						<?php bp_the_profile_field_name(); ?>
						<?php bp_the_profile_field_required_label(); ?>
					</label>
				</div>

				<div class="small-9 columns">
					<?php

					/** This action is documented in bp-xprofile/bp-xprofile-classes */
					do_action( bp_get_the_profile_field_errors_action() ); ?>

					<?php bp_the_profile_field_options( array( 'user_id' => $user_id ) );

					if ( ! bp_get_the_profile_field_is_required() ) : ?>

						<a class="clear-value" href="javascript:clear( '<?php echo esc_js( bp_get_the_profile_field_input_name() ); ?>' );">
							<?php esc_html_e( 'Clear', 'buddypress' ); ?>
						</a>

					<?php endif; ?>
				</div>
			</div>
		</div>

		<?php
	}

}
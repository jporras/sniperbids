<?php


/**
* Get membership fields IDs
* todo: make this dynamic
*/
function ultdww_get_membership_field_ids(){
	// 10 = Type
	// 14 = Paypal email
	echo ",10,14";
}

// add_action('bp_core_screen_signup', 'ultdww_core_screen_signup');

/**
 * Extend buddypress bp_xprofile_get_field_types();
 * Get details of all xprofile field types.
 *
 * @since 2.0.0
 *
 * @return array Key/value pairs (field type => class name).
 */
function ultdww_xprofile_get_field_types() {
	$fields = array(
		// 'checkbox'       => 'BP_XProfile_Field_Type_Checkbox',
		// 'datebox'        => 'BP_XProfile_Field_Type_Datebox',
		// 'multiselectbox' => 'BP_XProfile_Field_Type_Multiselectbox',
		// 'number'         => 'BP_XProfile_Field_Type_Number',
		// 'url'            => 'BP_XProfile_Field_Type_URL',
		'radio'          => 'ULTDWW_XProfile_Field_Type_Radiobutton',
		'selectbox'      => 'ULTDWW_XProfile_Field_Type_Selectbox',
		// 'textarea'       => 'BP_XProfile_Field_Type_Textarea',
		'textbox'        => 'ULTDWW_XProfile_Field_Type_Textbox',
	);

	/**
	 * Filters the list of all xprofile field types.
	 *
	 * If you've added a custom field type in a plugin, register it with this filter.
	 *
	 * @since 2.0.0
	 *
	 * @param array $fields Array of field type/class name pairings.
	 */
	return apply_filters( 'bp_xprofile_get_field_types', $fields );
}

/** 
 * Extend buddypress bp_xprofile_create_field_type();
 * Creates the specified field type object; used for validation and templating.
 *
 * @since 2.0.0
 *
 * @param string $type Type of profile field to create. See {@link bp_xprofile_get_field_types()} for default core values.
 *
 * @return object $value If field type unknown, returns BP_XProfile_Field_Type_Textarea.
 *                       Otherwise returns an instance of the relevant child class of BP_XProfile_Field_Type.
 */
function ultdww_xprofile_create_field_type( $type ) {

	$field = bp_xprofile_get_field_types();

	$ultdww_field = ultdww_xprofile_get_field_types();
	
	$bp_xprofile_class = isset( $field[$type] ) ? $field[$type] : '';

	/**
	 * To handle (missing) field types, fallback to a placeholder field object if a type is unknown.
	 */
	if ( $bp_xprofile_class && class_exists( $bp_xprofile_class ) ) {

		$bp_xprofile_instance = new $bp_xprofile_class;

		$ultdww_xprofile_class = isset( $ultdww_field[$type] ) ? $ultdww_field[$type] : '';

		if($ultdww_xprofile_class)
		{
			return new $ultdww_xprofile_class($bp_xprofile_instance);
		}

		// return buddy press instance by default.

		return new $bp_xprofile_instance;

	} else {
		return new BP_XProfile_Field_Type_Placeholder;
	}
}


/**
 * Render the navigation markup for the displayed user.
 */
function ultdww_get_displayed_user_nav() {
	$bp = buddypress();

	// add link for user's auction

	$bp->bp_nav[] = [
		'name' => 'Active Auctions',
      	'slug' => 'active-auctions',
      	'link' => get_site_url() . '/dashboard/?dashboard=manage-auctions',
      	'css_id' => 'active-auctions',
      	'show_for_displayed_user' => true,
      	'position' => 10,
      	// 'screen_function' => string 'bp_activity_screen_my_activity' (length=30)
      	// 'default_subnav_slug' => string 'just-me' (length=7)
	];

	foreach ( (array) $bp->bp_nav as $user_nav_item ) {
		if ( empty( $user_nav_item['show_for_displayed_user'] ) && !bp_is_my_profile() )
			continue;

		$selected = '';
		if ( bp_is_current_component( $user_nav_item['slug'] ) ) {
			$selected = ' class="current selected"';
		}

		if ( bp_loggedin_user_domain() ) {
			$link = str_replace( bp_loggedin_user_domain(), bp_displayed_user_domain(), $user_nav_item['link'] );
		} else {
			$link = trailingslashit( bp_displayed_user_domain() . $user_nav_item['link'] );
		}

		/**
		 * Filters the navigation markup for the displayed user.
		 *
		 * This is a dynamic filter that is dependent on the navigation tab component being rendered.
		 *
		 * @since 1.1.0
		 *
		 * @param string $value         Markup for the tab list item including link.
		 * @param array  $user_nav_item Array holding parts used to construct tab list item.
		 *                              Passed by reference.
		 */
		echo apply_filters_ref_array( 'bp_get_displayed_user_nav_' . $user_nav_item['css_id'], array( '<li id="' . $user_nav_item['css_id'] . '-personal-li" ' . $selected . '><a id="user-' . $user_nav_item['css_id'] . '" href="' . $link . '">' . $user_nav_item['name'] . '</a></li>', &$user_nav_item ) );
	}
}
